# Goomer Lista Rango

## Backend Challenge

### About

A Simple Nodejs API to manage restaurants and its products.

### Pre requisites

1. Nodejs version 10 and up
2. MongoDB Community Server
3. Cloudinary account

### Set up

Create the environment variable with the information of your cloudinary storage, api running port and local storage folder.

Use the following as template and replace with your own data:

```
PORT=<the_port_of_your_preference>
LOCAL_STORAGE_PATH='<the_directory_to_save_files_before_cloud_upload>'
CLOUD_NAME=<cloudnary_cloudname>
API_KEY=<cloudnary_api_key>
API_SECRET=<cloudnary_api_secret>
```

After that, run ``` npm install ``` to install all the dependencies from package.json.

Run the API with  ```node server.js ```

### Endpoints

[GET] List all restaurants
```
/restaurant/getall
```

[POST] Create restaurant
```
/restaurant/create
```

[GET] List data of one restaurant
```
/restaurant/get/:id
```

[PATCH] Update restaurant
```
/restaurant/update/:id
```

[DELETE] Delete restaurant
```
/restaurant/delete/:id
```

[GET] List all products of a restaurant
```
/restaurant/:restaurantid/products
```

[POST] Create product
```
/restaurant/:restaurantid/product/create
```

[PATCH] Update product
```
/restaurant/:restaurantid/product/:productid/update
```

[DELETE] Delete product
```
/restaurant/:restaurantid/product/:productid/delete
```

### Code structure

**Database**: MongoDB is used as database, Mongo is NoSQL database where data is stored in documents. The documents live inside the collections. A collection can have many documents and a database can have many collections. 

For this project, it is used a database with two collections (restaurants and products). 

A restaurant is stored inside a document and together with required fields it has an array of products ids.

The restaurant's products are stored in the products collection, and the document contais the id of the its restaurant.

**Server.js**: It is the entry point of the API and it is responsible for initializing the HTTP server and the database and storage services.

**dbIndex.js**: The database interactions are made using the Mongoose library. In this file the connection is started and a database is created (if not yet existed), aslo the mongoose models are defined.

**Routes**: The routes folder contains two files that holds all the endpoints of the API:
 - **restaurantRoute.js**:  It has all the routes related to mananging restaurant documents directly
 - **uploadFileRoute**: Contains a route to receive file uploads using the multer library

**Schemas**: This directory contains the schemas for the both product and restaurant. Mongoose Schemas are types of model that can be used to guarantee the data follows the specified patterns

**Services**: All the controlling logic is done by the services. 
- **dbService.js**: It does all the interactions with the database
- **storageService.js**: Responsible for interacting with the cloudinary storage
- **restaurantService.js**: It is the middleware of the actions with the restaurant, and it is here that business logic is added.
- **productService.js**: It is the middleware of the actions with the restaurant, and it is here that business logic is added.

### Challenges and difficulties

The biggest challenge was definitely the time I had to put effort on the project, as I currently work and study. 

For the required functionalites, the one that ended missing is the validation of the time interval (to always have at least a gap of 15 minutes) and also the deleteion of the media from the cloudinary storage. Unfortunately I don't have much experience with testing, so this project doesn't apply unit testing.

Further improvements on the code include better error/response handling, implement testing to give more stability, refactor dbService to create more reusable code and reduce repetitive one, 

