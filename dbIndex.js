const mongoose = require('mongoose')

module.exports = mongoose.connect(`mongodb://localhost:27017/goomerChallengeDB`,{
    useNewUrlParser: true,
    useUnifiedTopology: true
})

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log('Connected to db')
});

const restaurantSchema = require('./schemas/restaurantSchema')
const productSchema = require('./schemas/productSchema')

mongoose.model('restaurants',restaurantSchema)
var Restaurant = mongoose.model('restaurants')

mongoose.model('products',productSchema)
var Product = mongoose.model('products')


module.exports = {Restaurant, Product}