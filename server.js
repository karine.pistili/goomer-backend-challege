require('dotenv').config()
require('./dbIndex')
require('./services/storageService')

const http = require('http')
const port = process.env.PORT || 5000
const app = require('./index')

const server = http.createServer(app)

server.listen(port)