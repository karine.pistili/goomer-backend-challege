const restaurantSchema = {
    name: { type: String, require: true },
    imgUrl: { type: String, require: true },
    address: {
        street: { type: String, require: true },
        number: { type: Number, require: true },
        city: { type: String, require: true },
        country: { type: String, require: true },
        complement: { type: String }
    },
    openingHours: [
        {
            day: { type: String, require: true },
            openTime: { type: String, require: true },
            closeTime: { type: String, require: true }
        }
    ],
    products: [
        {
            productId: {type: String, require:true}
        }
    ]
}

module.exports = restaurantSchema