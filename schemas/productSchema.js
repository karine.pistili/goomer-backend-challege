const productSchema = {
    name: {type: String, require: true},
    imgUrl: {type: String, require: true},
    price: {type: Number, require:true},
    category: {type: String, require:true},
    restaurantId: {type:String, require:true},
    sale: {
        description: {type: String},
        salePrice: {type: Number},
        saleTime:[
            {
                day: { type: String, require: true },
                startTime: { type: String, require: true },
                endTime: { type: String, require: true }
            }
        ]
    }
}

module.exports = productSchema