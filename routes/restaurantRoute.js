const express = require('express')
const router = express.Router()
const restaurantService = require('../services/restaurantService')
const productService = require('../services/productService')

router.get('/getall', (req, res, next) => {
    restaurantService.getAllRestaurants()
        .then((array) => {
            res.status(200).send(array)
        })
        .catch((err) => {
            res.status(500).json(err)
        })
})

router.post('/create', (req, res, next) => {
    let restaurant = req.body
    restaurantService.createRestaurant(restaurant)
        .then((response) => {
            res.status(response.status).json(response)
        })
        .catch((err) => {
            res.status(err.status).json(err)
        })
})

router.get('/get/:id', (req, res, next) => {
    var id = req.params.id
    restaurantService.getRestaurantById(id)
        .then((response) => {
            res.status(200).json(response)
        })
        .catch((err) => {
            res.status(500).json(err)
        })
})

router.delete('/delete/:id', (req, res, next) => {
    var id = req.params.id
    restaurantService.deleteRestaurant(id)
        .then((response) => {
            res.status(response.status).json(response)
        })
        .catch((err) => {
            res.status(err.status).json(err)
        })
})

router.patch('/update/:id', (req, res, next) => {
    var id = req.params.id
    var data = req.body
    restaurantService.updateRestaurant({id: id, data: data})
    .then((response) => {
        res.status(response.status).json(response)
    })
    .catch((err) => {
        res.status(err.status).json(err)
    })
})

router.get('/:id/products', (req, res, next) => {
    var restaurantId = req.params.id
    productService.getAllProducts(restaurantId)
        .then((array) => {
            res.status(200).send(array)
        })
        .catch((err) => {
            res.status(500).json(err)
        })
})

router.post('/:id/product/create', (req, res, next) => {
    let product = req.body
    productService.createProduct(product)
        .then((response) => {
            res.status(response.status).json(response)
        })
        .catch((err) => {
            res.status(err.status).json(err)
        })
})

router.patch('/:restaurantid/product/:productid/update', (req, res, next) => {
    var id = req.params.productid
    var data = req.body
    productService.updateProduct({id: id, data: data})
    .then((response) => {
        res.status(response.status).json(response)
    })
    .catch((err) => {
        res.status(err.status).json(err)
    })
})

router.delete('/:restaurantid/product/:productid/delete', (req, res, next) => {
    var restaurantId = req.params.restaurantid
    var productId = req.params.productid

    productService.deleteProduct(restaurantId,productId)
    .then((response) =>{
        res.status(response.status).send(response)
    })
    .catch((err) =>{
        res.status(err.status).send(err)
    })
})



module.exports = router