const express = require('express')
const router = express.Router()
const multer = require('multer')
const storageService = require('../services/storageService')

// configure localstorage for files using multer
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, `${process.env.LOCAL_STORAGE_PATH}`)
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname)
    }
})


const upload = multer({ storage: storage })

router.post("/upload", upload.single('media'), (req, res, next) => {
    storageService.uploadFile(req.file, req.body)
    .then((response) =>{
        res.status(response.status).send(response)
    })
    .catch((error) =>{
        res.status(error.status).send(error)
    })
})


module.exports = router