const database = require('../services/dbService')

module.exports = {
    createProduct: async function (product) {
        if (product.openingHours != null && product.openingHours != undefined) {
            var validTime = validateOpeningHours(data.openingHours)

            if (validTime) {
                return await database.createProduct(product)
            }
            return new Promise((resolve, reject) => { reject({ status: 400, msg: 'Invalid time format' }) })
        }
        return await database.createProduct(product)

    },
    getAllProducts: async function (restaurantId) {
        return await database.getAllProducts(restaurantId)
    },
    updateProduct: async function (data) {
        if (data.openingHours != null && data.openingHours != undefined) {
            var validTime = validateOpeningHours(data.openingHours)

            if (validTime) {
                return await database.updateProduct(data)
            }
            return new Promise((resolve, reject) => { reject({ status: 400, msg: 'Invalid time format' }) })
        }
        return await database.updateProduct(data)
    },
    deleteProduct: async function (restaurantId,productId) {
        return await database.deleteProduct(restaurantId,productId)
    }
}