const database = require('../services/dbService')
const storage = require('../services/storageService')

module.exports = {
    createRestaurant: async function (restaurant) {
        if (restaurant.openingHours != null && restaurant.openingHours != undefined) {
            var validTime = validateOpeningHours(restaurant.openingHours)

            if (validTime) {
                return await database.createRestaurant(restaurant)
            }
            return new Promise((resolve, reject) => { reject({ status: 400, msg: 'Invalid time format' }) })
        }
        return await database.createRestaurant(restaurant)
    },
    getAllRestaurants: async function () {
        return await database.getAllRestaurants()
    },
    getRestaurantById: async function (id) {
        return await database.getRestaurantById(id)
    },
    deleteRestaurant: async function (id) {
        return await database.deleteRestaurant(id)
    },
    updateRestaurant: async function (data) {
        if (data.openingHours != null && data.openingHours != undefined) {
            var validTime = validateOpeningHours(data.openingHours)

            if (validTime) {
                return await database.updateRestaurant(data)
            }
            return new Promise((resolve, reject) => { reject({ status: 400, msg: 'Invalid time format' }) })
        }
        return await database.updateRestaurant(data)
    }
}


function validateOpeningHours(openHours) {
    var valid

    //validate format
    openHours.every((o) => {
        if (!validateTimeFields(o.openTime)) {
            valid = false
            return false
        }
        else if (!validateTimeFields(o.closeTime)) {
            valid = false
            return false
        }
        valid = true
        return true
    })

    return valid
}

function validateTimeFields(time) {
    if (time.length != 5) return false
    if (time.substring(2, 3) != ":") return false
    var hour = time.substring(0, 2)
    var minute = time.substring(3)
    if (hour < 0 || hour >= 24) return false;
    if (minute < 0 || minute >= 60) return false;
    return true;
}