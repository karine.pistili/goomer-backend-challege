const cloudinary = require('cloudinary').v2
const restaurantService = require('../services/restaurantService')
const productService = require('../services/productService')
const fs = require('fs')

cloudinary.config({
    cloud_name: process.env.CLOUD_NAME,
    api_key: process.env.API_KEY,
    api_secret: process.env.API_SECRET
});


module.exports = {
    uploadFile: async function (file, data) {
        return new Promise((resolve, reject) => {
            let resultUrl, resultId, public_id

            if (data.type == 'restaurant')
                public_id = data.restaurantId
            else if (data.type == 'product')
                public_id = data.productId

            cloudinary.uploader.upload(`${process.env.LOCAL_STORAGE_PATH}/${file.filename}`,
                { public_id: public_id },
                function (error, result) {
                    resultUrl = result.url
                    resultId = result.public_id
                }).then(async () => {
                    if (data.type == 'restaurant') {
                        restaurantService.updateRestaurant({ id: data.restaurantId, data: { imgUrl: resultUrl, imgId: resultId } })
                            .then((response) => {
                                deleteFromLocalFolder(`${process.env.LOCAL_STORAGE_PATH}/${file.filename}`)
                                resolve(response)
                            })
                            .catch((err) => {
                                console.log(err)
                                reject(err)
                            })
                    }
                    else if (data.type == 'product') {
                        productService.updateProduct({ id: data.productId, data: { imgUrl: resultUrl } })
                            .then((response) => {
                                deleteFromLocalFolder(`${process.env.LOCAL_STORAGE_PATH}/${file.filename}`)
                                resolve(response)
                            })
                            .catch((err) => {
                                reject(err)
                            })
                    }
                }).catch((err) => {
                    reject({ status: 500, msg: 'Unable to upload file' })
                })
        })
    },
    //wip
    deleteFileFromCloud: async function (publicId) {
        return new Promise((resolve, reject) => {
            cloudinary.uploader.destroy(`${publicId}`, function (error, result) {
                console.log('Deleting from cloud', result, error)
            });
        })
    },


}



function deleteFromLocalFolder(path) {
    try {
        fs.unlinkSync(path)
    } catch (err) {
        console.error(err)
    }
}

