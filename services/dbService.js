const Schemas = require('../dbIndex')

module.exports = {
    createRestaurant: function (restaurant) {
        var r = new Schemas.Restaurant(restaurant)

        return new Promise((resolve, reject) => {
            r.save().then((doc) => {
                console.log('Document created with success: ', doc)
                resolve({ status: 201, msg: `Success. Document created with success ${doc._id}` })
            })
                .catch((err) => {
                    console.log('Error on creating document: ', err)
                    resolve({ status: 500, msg: `Internal Server Error. Error on creating document ${err}` })
                })
        })
    },
    createProduct: function (product){
        var p = new Schemas.Product(product)
        return new Promise((resolve, reject) =>{
            p.save().then((doc) => {
                console.log('Document created with success: ', doc)
                this.pushProductToRestaurantArray(product.restaurantId,doc._id)
                resolve({ status: 201, msg: `Success. Document created with success ${doc._id}` })
            })
                .catch((err) => {
                    console.log('Error on creating document: ', err)
                    resolve({ status: 500, msg: `Internal Server Error. Error on creating document ${err}` })
                })
        })
    },
    pushProductToRestaurantArray(restaurantId, productId){
        Schemas.Restaurant.updateOne(
            { _id: restaurantId }, 
            { $push: { products: {'productId' : productId}} },
            (err,writeOpResult) =>{
                console.log(writeOpResult,err)
            }
        )
    },
    getAllRestaurants: async function () {
        const array = await Schemas.Restaurant.find({})
        console.log('Retrieved all docs with success', array)

        return array
    },
    getAllProducts: async function (id){
        const array = await Schemas.Product.find({ restaurantId: { $eq: `${id}` }  } ,function(err, docs){
            console.log(docs);
         })
        console.log('Retrieved all docs with success', array)

        return array
    },
    getRestaurantById: async function (id) {
        var document
        var response
        try {
            document = await Schemas.Restaurant.find({ _id: id })
        }
        catch {
            return { status: 400, msg: `Bad Request. There is something wrong with your request` }
        }

        if (document.length == 0) {
            console.log('Document not found')
            response = { status: 404, msg: `Not found. Document ${id} not found` }
        }
        else {
            console.log('Retrieved doc with success', document)
            response = document
        }

        return response
    },
    deleteRestaurant: async function (id) {
        let response
        try {
            const res = await Schemas.Restaurant.deleteOne({ _id: id })
            if (res.deletedCount == 0) {
                return response = { status: 404, msg: `Not found. Document not found ${id}` }
            }
            else {
                console.log('Document deleted with success', id)
                return response = { status: 200, msg: `Success. Document ${id} deleted with success` }
            }
        }
        catch (e) {
            return response = { status: 400, msg:`Bad Request. ${e.reason.message}` }
        }
    },
    deleteProduct: async function (restaurantId, productId){
        try {
            const res = await Schemas.Product.deleteOne({ _id: productId })
            if (res.deletedCount == 0) {
                return { status: 404, msg: `Not found. Document not found ${productId}` }
            }
            else {
                console.log('Document deleted with success', productId)
                this.popProductFromCommerceArray(restaurantId,productId)
                return { status: 200, msg: `Success. Document ${productId} deleted with success` }
            }
        }
        catch (e) {
            return { status: 400, msg:`Bad Request. ${e.reason.message}` }
        }
    },

    popProductFromCommerceArray(restaurantId,productId){
        Schemas.Restaurant.updateOne(
            { _id: restaurantId }, 
            { $pull: { products: {'productId' : productId}} },
            (err,writeOpResult) =>{
                console.log(writeOpResult,err)
            }
        )
    },

    updateRestaurant: async function(obj){
        return new Promise ((resolve,reject) =>{
            Schemas.Restaurant.updateOne({_id: obj.id},{ $set : obj.data}, (err,writeOpResult) =>{
                if(!err){
                    if(writeOpResult.nModified == 0){
                        resolve({ status: 404, msg: `Not found. Document not found ${obj.id}` })
                    }
                    else{
                        resolve({status: 200, msg: `Success. Document updated with success`})
                    }
                }
                else{
                    console.log('Unable to update document',err)
                    reject({status: 500, msg: `Internal Server Error. ${err.reason.message}`})
                }
            } )
        })
    },
    updateProduct: async function(obj){
        return new Promise ((resolve,reject) =>{
            Schemas.Product.updateOne({_id: obj.id},{ $set : obj.data}, (err,writeOpResult) =>{
                if(!err){
                    if(writeOpResult.nModified == 0){
                        resolve({ status: 404, msg: `Not found. Document not found ${obj.id}` })
                    }
                    else{
                        resolve({status: 200, msg: `Success. Document updated with success`})
                    }
                }
                else{
                    console.log('Unable to update document',err)
                    reject({status: 500, msg: `Internal Server Error. Unable to update document ${err}`})
                }
            } )
        })
    }
}