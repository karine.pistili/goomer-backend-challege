const express = require('express')
const bodyParser = require('body-parser')
const app = express()

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

// configure the response headers
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With,Content-Type,Accept,Authorization')
    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET')
        return res.status(200).json({})
    }
    next()
})

// routes
const restaurantRoutes = require('./routes/restaurantRoute')
const mediaRoutes = require('./routes/uploadFileRoute')
app.use('/restaurant',restaurantRoutes)
app.use('/media',mediaRoutes)

// couldn't find a route
app.use((req, res, next) => {
    const error = new Error('Route not found')
    error.status = 404
    next(error)
})

// handles all errors from other places that weren't treated
app.use((error, req, res, next) => {
    res.status(error.status || 500)
    res.json({
        error: {
            status: error.status,
            msg: error.message
        }
    })
})

module.exports = app
